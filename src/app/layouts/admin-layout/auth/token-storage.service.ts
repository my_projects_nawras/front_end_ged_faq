import {Injectable} from '@angular/core';

const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';
const NAME_KEY = 'Authname';
const EMAIL_KEY = 'Authemail';
const ID_KEY = 'Autheid';


@Injectable({
    providedIn: 'root'
})
export class TokenStorageService {
    private roles: Array<string> = [];

    constructor() {
    }

    signOut() {
        window.sessionStorage.clear();
    }

    public saveEmail(email: any) {
        window.sessionStorage.removeItem(EMAIL_KEY);
        window.sessionStorage.setItem(EMAIL_KEY, email);
    }

    public getEmail(): any {
        return sessionStorage.getItem(EMAIL_KEY);
    }

    public saveId(id: any) {
        window.sessionStorage.removeItem(ID_KEY);
        window.sessionStorage.setItem(ID_KEY, id);
    }

    public getId(): any {
        return sessionStorage.getItem(ID_KEY);
    }

    public saveToken(token: string) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }

    public getToken(): string {
        return sessionStorage.getItem(TOKEN_KEY);
    }

    public saveUsername(username: string) {
        window.sessionStorage.removeItem(USERNAME_KEY);
        window.sessionStorage.setItem(USERNAME_KEY, username);
    }

    public getUsername(): string {
        return sessionStorage.getItem(USERNAME_KEY);
    }


    public saveAuthorities(authorities: any[]) {
        console.log("authorities fram save : "+JSON.stringify(authorities))
        window.sessionStorage.removeItem(AUTHORITIES_KEY);
        window.sessionStorage.setItem(AUTHORITIES_KEY,  JSON.stringify(authorities));
    }

    public getAuthorities(): any[] {
        this.roles = [];

        if (sessionStorage.getItem(TOKEN_KEY)) {

            //localDataStorage.getAuthorities():
            console.log("role pus"+sessionStorage.getItem(AUTHORITIES_KEY))
            return JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY));

        }
//console.log("role pus"+this.roles)
        return JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY));
    }
//     public getAuthorities(): string[] {
//         this.roles = [];
//
//         if (sessionStorage.getItem(TOKEN_KEY)) {
//
//             this.roles.push(sessionStorage.getItem(AUTHORITIES_KEY));
//
//         }
//
//         return this.roles;
//     }

}
