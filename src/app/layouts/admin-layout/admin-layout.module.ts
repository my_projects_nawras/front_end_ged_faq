import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminLayoutRoutes} from './admin-layout.routing';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {DocumentComponent} from 'app/layouts/admin-layout/document/document.component';
import {RepertoireComponent} from 'app/layouts/admin-layout/document/repertoire/repertoire.component';
import { FaqComponent } from './faq/faq.component';
import {
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule,
    MatExpansionModule
} from '@angular/material';
import {MatDialogModule} from "@angular/material/dialog";

import {MatIconModule} from "@angular/material/icon";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatCardModule} from "@angular/material/card";
import {RepertoireService} from "./services/repertoire.service";
import {DocumentService} from "./services/document.service";
import {MatMenuModule} from "@angular/material/menu";
import {ReplacedocumentComponent} from "./replacedocument/replacedocument.component";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {ConstantsService} from "../../common/services/constants.service";
import {EditDocumentComponent} from "./edit-document/edit-document.component";
import {NotificationService} from "./services/notification.service";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {SearchdocumentService} from "./services/searchdocument.service";
import {PageService} from "./services/page.service";
import {DropDownEditorComponent} from "./drop-down-editor/drop-down-editor.component";
import {AgGridModule} from "ag-grid-angular";
import {DetailDocumentComponent} from './detail-document/detail-document.component';
import {DeletDocumentComponent} from "./delet-document/delet-document.component";
import {MatListModule} from "@angular/material/list";
import {DocumentSearchComponent} from "./document-search/document-search.component";
import {LivrePipe} from "./pipes/livre.pipe";
import {UserService} from "./services/user.service";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {UploadService} from "./services/upload.service";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {ComponentsModule} from "../../components/components.module";
import {ProfileComponent} from "./profile/profile.component";
import {FaqService} from "./services/faq.service";
import {DeleteQuestionComponent} from "./delete-question/delete-question.component";
import {EditeQuestionComponent} from "./edite-question/edite-question.component";


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatDialogModule,
        MatIconModule,
        MatSelectModule,
        MatExpansionModule,
        MatProgressBarModule,
        MatCardModule,
        MatMenuModule,
        MatToolbarModule,
        MatGridListModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        AgGridModule.withComponents(
            [DropDownEditorComponent]
        ),
        MatListModule,
        MatProgressSpinnerModule,
        ComponentsModule,


    ],
    schemas: [NO_ERRORS_SCHEMA],
    declarations: [
        DashboardComponent,
        TableListComponent,
        TypographyComponent,
        IconsComponent,
        RepertoireComponent,
        DocumentComponent,
        ReplacedocumentComponent,
        EditDocumentComponent,
        DropDownEditorComponent,
        DetailDocumentComponent,
        DeletDocumentComponent,
        DocumentSearchComponent,
        LivrePipe,
        LoginComponent,
        RegisterComponent, ProfileComponent,FaqComponent,DeleteQuestionComponent,
        EditeQuestionComponent,
    ],
    providers: [MatDialogModule,FaqService, RepertoireService, DocumentService, ConstantsService, NotificationService, SearchdocumentService, PageService, LivrePipe, UserService, UploadService],
    entryComponents: [FaqComponent,ProfileComponent,DeleteQuestionComponent,
        EditeQuestionComponent, RepertoireComponent, ReplacedocumentComponent, EditDocumentComponent, DropDownEditorComponent, DocumentComponent, DetailDocumentComponent, DeletDocumentComponent, DocumentSearchComponent, LoginComponent,
        RegisterComponent,]
})


export class AdminLayoutModule {
}
