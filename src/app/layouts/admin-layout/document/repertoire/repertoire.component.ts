import {Component, OnInit} from '@angular/core';
import {RepertoireService} from 'app/layouts/admin-layout/services/repertoire.service';
import {Repertoire} from 'app/layouts/admin-layout/entity/repertoire';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {Router} from "@angular/router";
import {TokenStorageService} from "../../auth/token-storage.service";

@Component({
    selector: 'app-repertoire',
    templateUrl: './repertoire.component.html',
    styleUrls: ['./repertoire.component.scss']
})
export class RepertoireComponent implements OnInit {
    repertoire: Repertoire[];
    repertoires: Repertoire = new Repertoire();
    panelOpenState = false;

    constructor(private token: TokenStorageService, private repertoireservice: RepertoireService,
                private router: Router) {
    }

    id_user: number = this.token.getId();

    ngOnInit() {

    }

    save(data) {
        console.log("repertoire", data);
        this.repertoireservice.postRepertoire(data.nomRepertoire, this.id_user)
            .subscribe(
                (data: any) => {
                    //this.repertoires = data.nomRepertoire
                    // this.router.navigate(['doc' +
                    // 'ument'])

                    console.log("success")

                },
                (err) => {
                    console.warn("an error has occured", err)
                },
                () => {
                    this.getAllRepertoire();
                });

    }

    getAllRepertoire() {
        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {
                this.repertoire = response['repertoires'];

            },
            err => console.log(err),
            () => {
                console.log(this.repertoire);
            }
        );
    }


}
