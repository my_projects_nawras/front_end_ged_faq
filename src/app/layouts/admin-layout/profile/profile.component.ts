import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {AuthService} from "../auth/auth.service";
import {TokenStorageService} from "../auth/token-storage.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    profileForm = new FormGroup({
            signupFormModalUsername: new FormControl(this.tokenStorage.getUsername(), Validators.required),
            loginFormModalPasswordOld: new FormControl('', [Validators.minLength(5)]),
            loginFormModalPasswordNew: new FormControl('', [Validators.minLength(5)])

        }
    );

    constructor(private userService: UserService, private router: Router, private auth: AuthService, private tokenStorage: TokenStorageService) {
    }

    username = '';
    showSpinner = false;
    showSuccess = false;
    showFail = false;

    ngOnInit() {
    }

    passItpass(): boolean {
        return this.profileForm.value.loginFormModalPasswordNew !== '' && this.profileForm.value.loginFormModalPasswordOld !== '';
    }

    onSauvegarder() {


        if (this.passItpass()) {
            const oldPass = this.profileForm.value.loginFormModalPasswordOld;
            const newPass = this.profileForm.value.loginFormModalPasswordNew;

            this.userService.updatePassword(this.username, oldPass, newPass).subscribe(
                res => {
                    console.log(res);
                    this.showSuccess = true;
                    this.showFail = false;
                    this.auth.attemptAuth(this.profileForm.value.loginFormModalPasswordNew).subscribe(
                        res2 => {
                            this.tokenStorage.saveToken(res2.accessToken);
                            this.tokenStorage.saveUsername(res2.username);
                        }
                    );

                },
                err => {
                    console.log(err);
                    this.showSuccess = false;
                    this.showFail = true;
                },
                () => {
                    // this.clearFields();
                    this.profileForm.value.signupFormModalUsername = this.tokenStorage.getUsername();
                }
            );
        }
    }

    clearFields() {
        this.profileForm.value.loginFormModalPasswordOld = '';
        this.profileForm.value.loginFormModalPasswordNew = '';
        // this.profileForm.reset();
    }
}
