import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplacedocumentComponent} from './replacedocument.component';

describe('ReplacedocumentComponent', () => {
    let component: ReplacedocumentComponent;
    let fixture: ComponentFixture<ReplacedocumentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ReplacedocumentComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ReplacedocumentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
