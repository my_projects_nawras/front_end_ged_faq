import {Repertoire} from "./repertoire";

export class Document {
    public id: any;
    public description: string;
    public dateCreation: Date;
    public nomDoc: string;
    public taille: number;
    public type: string;
    public repertoire: Repertoire;
}
