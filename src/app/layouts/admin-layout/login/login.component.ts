import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {AuthService} from '../auth/auth.service';
import {TokenStorageService} from '../auth/token-storage.service';
import {AuthLoginInfo} from '../auth/login-info';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";
import {User} from "../entity/user";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    form: any = {};
    isLoggedIn = false;
    isLoginFailed = false;
    errorMessage = '';
    roles: String [];
    id: any;
    private loginInfo: AuthLoginInfo;
    showSpinner = false;

    constructor(private userService: UserService, private auth: AuthService, private router: Router, private tokenStorage: TokenStorageService) {
    }

    ngOnInit() {
        if (this.tokenStorage.getToken()) {
            this.isLoggedIn = true;
            this.roles = this.tokenStorage.getAuthorities();

        }
    }

    //
    // onSubmit() {
    //     console.log(this.form);
    //
    //     this.loginInfo = new AuthLoginInfo(
    //         this.form.username,
    //         this.form.password);
    //
    //     this.auth.signin(this.loginInfo).subscribe(
    //         data => {
    //             console.log(data)
    //             this.tokenStorage.saveToken(data.accessToken);
    //             this.tokenStorage.saveUsername(data.username);
    //             console.log(data.username);
    //             this.tokenStorage.saveId(data.id);
    //             this.id =this.tokenStorage.saveId(data.id);
    //
    //             console.log(data.id);
    //
    //             this.tokenStorage.saveAuthorities(data.authorities);
    //
    //             this.isLoginFailed = false;
    //             this.isLoggedIn = true;
    //             this.roles = this.tokenStorage.getAuthorities();
    //             this.reloadPage();
    //         },
    //         error => {
    //             console.log(error);
    //             this.errorMessage = error.error.message;
    //             this.isLoginFailed = true;
    //         }
    //     );
    // }
    //
    // reloadPage() {
    //     window.location.reload();
    // }
    //
    // recuperationId(): any{
    //
    //     return this.id;
    //
    // }
}
