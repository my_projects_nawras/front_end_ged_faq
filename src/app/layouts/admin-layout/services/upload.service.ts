import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UploadService {

    constructor(private http: HttpClient) {
    }

    URL_BASE = 'http://localhost:8080';
    URL_POST = '/post';
    URL_GET_ALL = '/getallfiles';
    switchImageEvent = new EventEmitter<string>();

    pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
        const formdata: FormData = new FormData();

        formdata.append('file', file);

        const req = new HttpRequest('POST', this.URL_BASE + this.URL_POST, formdata, {
            reportProgress: true,
            responseType: 'text'
        });

        return this.http.request(req);
    }

    getFiles(): Observable<any> {
        return this.http.get(this.URL_BASE + this.URL_GET_ALL);
    }

    imgChanged(imgName: string) {
        this.switchImageEvent.emit(imgName);
    }
}
