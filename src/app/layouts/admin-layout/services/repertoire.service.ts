import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Repertoire} from 'app/layouts/admin-layout/entity/repertoire';
import {environment} from 'environments/environment';
import {Observable} from "rxjs";
import {Document} from "../entity/document";
import {TokenStorageService} from "../auth/token-storage.service";


@Injectable({
    providedIn: 'root'
})
export class RepertoireService {
    URL_BASE = environment.apiBaseUrl;
    id_user: number = this.token.getId();
    URL_REPERTOIRE = '/create';
    URL_GET = "/getAllRepertoire";
    reper: Repertoire[];

    constructor(private token: TokenStorageService, private httpClient: HttpClient) {
    }

    getAllRepertoire(id_user: number) {
        const token = this.token.getToken();
        // const header=new HttpHeaders({Authorization:token});
        const httpOptions = {headers: new HttpHeaders};
        return this.httpClient.get<Repertoire[]>(this.URL_BASE + "/" + id_user + this.URL_GET, {
            headers: {Authorization: `Bearer ${token}`},
            responseType: 'json'
        });
    }

    postRepertoire(nomRepertoire: string, id_user: number) {
        console.log(nomRepertoire);
        const token = this.token.getToken();
        const params = new HttpParams().set('nomRepertoire', nomRepertoire.toString());

        return this.httpClient.get(this.URL_BASE + '/' + id_user + this.URL_REPERTOIRE, {
            params,
            headers: {Authorization: `Bearer ${token}`}
        });
    }

    editDocument(id: number, repertoire: Repertoire) {
        const token = this.token.getToken();
        return this.httpClient.put(this.URL_BASE + '/' + id, repertoire)

    }
}
