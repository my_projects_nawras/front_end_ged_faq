import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Document} from "../entity/document";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable, of} from "rxjs";
import {environment} from "../../../../environments/environment";
import {catchError, map, tap} from "rxjs/operators";
import {TokenStorageService} from "../auth/token-storage.service";

@Injectable({
    providedIn: 'root'
})
export class DocumentService {
    URL_BASE = environment.apiUrl;

    URL_Doc = '/get';
    URL_POST = '/post';
    URL_FILES = '/filesToRepertoire';
    URI_Replace = '/deplaceDocument/'
    form: FormGroup;

    constructor(public fb: FormBuilder, private httpClient: HttpClient, private token: TokenStorageService) {

    }

    uploadFile(formData: FormData, id_user: number) {
        let httpOptions = {headers: new HttpHeaders, responseType: 'text' as 'json'};
        const token = this.token.getToken();
        return this.httpClient.post<any>(this.URL_BASE + "/" + id_user + this.URL_POST, formData, {
            headers: {Authorization: `Bearer ${token}`},
            responseType: 'text' as 'json'
        });


    }

    getAllDocument(id_user: number) {
        const token = this.token.getToken();
        return this.httpClient.get("http://localhost:8080/document/" + id_user + "/get", {headers: {Authorization: `Bearer ${token}`}});

    }

    uploadMultiFile(formData: File) {
        return this.httpClient.post(this.URL_BASE + this.URL_FILES, formData, {responseType: 'text'});
    }

    deleteDocument(id: number): Observable<any> {
        const token = this.token.getToken();
        return this.httpClient.delete("http://localhost:8080/document/delete/" + id, {headers: {Authorization: `Bearer ${token}`}});
    }

    replaceDocument(id: number, formData: FormData): Observable<any> {
        const token = this.token.getToken();
        console.log("idfromdeplacer : ", this.URL_BASE + this.URI_Replace + id);
        console.log("formdata", formData)
        return this.httpClient.post(this.URL_BASE + this.URI_Replace + id, formData, {headers: {Authorization: `Bearer ${token}`}});
    }

    editDocument(id: number, nomDoc: string) {
        const token = this.token.getToken();

        return this.httpClient.put("http://localhost:8080/document/edite/" + id, nomDoc, {headers: {Authorization: `Bearer ${token}`}})
    }


    detailsContent(id: number) {
        const token = this.token.getToken();
        return this.httpClient.get("http://localhost:8080/document/details/" + id, {
            headers: {Authorization: `Bearer ${token}`},
            responseType: "arraybuffer"
        })
    }

    downloadFile(id: number) {
        const token = this.token.getToken();
        return this.httpClient.get("http://localhost:8080/document/download/" + id, {
            headers: {Authorization: `Bearer ${token}`},
            responseType: "arraybuffer"
        })
    }

    searchHeroes(nomDoc: string, id_user: number) {
        const token = this.token.getToken();
        const url = `${this.URL_BASE}/` + id_user + `/search/name?contains=${nomDoc}`;
        return this.httpClient.get<any>(url, {headers: {Authorization: `Bearer ${token}`}});

    }

    deletContentDirectory(codeRep: string) {
        const token = this.token.getToken();
        return this.httpClient.delete("http://localhost:8080/document/" + codeRep, {headers: {Authorization: `Bearer ${token}`}})
    }
}
