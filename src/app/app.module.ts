import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';
import {AppComponent} from './app.component';


import {
    AgmCoreModule
} from '@agm/core';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {UserWelcomeComponent} from './user-welcome/user-welcome.component';
import {UserWelcomeContentComponent} from './user-welcome-content/user-welcome-content.component';
import {NavbarLoginComponent} from './navbar-login/navbar-login.component';
import {NavbaComponent} from './navba/navba.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {NavbarUserWelcomeComponent} from './navbar-user-welcome/navbar-user-welcome.component';
import {SigninModalComponent} from './signin-modal/signin-modal.component';
import {SignupModalComponent} from './signup-modal/signup-modal.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { AjoutQuestionComponent } from './ajout-question/ajout-question.component';




@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
        }),
        MatProgressSpinnerModule,
    ], schemas: [NO_ERRORS_SCHEMA],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        UserWelcomeComponent,
        UserWelcomeContentComponent,
        NavbarLoginComponent,
        NavbaComponent,
        LandingPageComponent,
        NavbarUserWelcomeComponent,
        SigninModalComponent,
        SignupModalComponent,
        AjoutQuestionComponent,




    ],
    providers: [],
    bootstrap: [AppComponent],
    //entryComponents:[RepertoireComponent]
})
export class AppModule {
}
